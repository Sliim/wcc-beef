beef CHANGELOG
==============

This file is used to list changes made in each version of the beef cookbook.

0.2.0
-----
- Chef 12 compatibility
- Use Rake instead of Strainer
- Test hardness (rubocop, chefspec, kitchen)
- No cookbook dependencies

0.1.1
-----
- Attributes reorganization

0.1.0
-----
- Initial release of beef cookbook

